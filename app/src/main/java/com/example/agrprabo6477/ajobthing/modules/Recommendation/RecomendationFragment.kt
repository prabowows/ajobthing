package com.example.agrprabo6477.ajobthing.modules.Recommendation

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.opengl.Visibility
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.agrprabo6477.ajobthing.R
import com.example.agrprabo6477.ajobthing.models.SearchViewModel
import com.example.agrprabo6477.ajobthing.models.response.AvailableJobResponse
import com.example.agrprabo6477.ajobthing.models.response.JobData
import com.example.agrprabo6477.ajobthing.models.response.JobResponse
import com.example.agrprabo6477.ajobthing.modules.DetailJob.DetailJobActivity
import com.example.agrprabo6477.ajobthing.modules.Favorite.FavoriteFragment
import com.example.agrprabo6477.ajobthing.modules.MainActivity
import com.example.agrprabo6477.ajobthing.utils.JobHelper
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_main.view.*
import id.ai.mobilerecruitment.utils.Helpers
import kotlinx.android.synthetic.main.fragment_main.*
import java.text.SimpleDateFormat

class RecomendationFragment : Fragment() {

    private lateinit var realm: Realm
    private lateinit var presenter: RecomendationPresenter
    private var isFromOnResume = false
    var dummy = ""
    private lateinit var filterViewModel: SearchViewModel
    private var list: MutableList<JobData> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_main, container, false)

        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as MainActivity).let {
            filterViewModel = ViewModelProviders.of(it).get(SearchViewModel::class.java)
            filterViewModel.currentFilterText.observe(this, Observer {
                it?.let {
                    //                    adapter.filter.filter(it)
                    if ((activity as MainActivity).CURRENT_ACTIVE_FRAGMENT.equals(
                            FavoriteFragment::class.java.simpleName,
                            true
                        )
                    ) {

                        setAdapter(list, it)
                        dummy = it
//                            sv_forum.visibility = View.VISIBLE
//                            list.clear()
//                            page = 1
//                            presenter.hitDataForum(page, keySearch = it)
                    }
                }

            })
        }

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        realm = Realm.getDefaultInstance()
        presenter = RecomendationPresenter(this, realm)
        try_again.setOnClickListener {
            presenter.hitGetData()
            Helpers.startShimmerView(sv_job,rv_recomendation)
            tv_error_internet.visibility = View.GONE
        }
        //presenter.hitGetData()
        //Helpers.showToast(this.requireContext(),dummy,true)

    }

//    override fun onResume() {
//        super.onResume()
//        presenter.hitGetData()
//        isFromOnResume = true
//        Helpers.startShimmerView(sv_job,rv_recomendation)
//    }

    override fun onStart() {
        super.onStart()
        presenter.hitGetData()
        tv_error_internet.visibility = View.GONE
        isFromOnResume = true
        Helpers.startShimmerView(sv_job, rv_recomendation)

    }


    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(sectionNumber: String): RecomendationFragment {
            val fragment = RecomendationFragment()
            //val args = Bundle()
            //args.putInt(ARG_SECTION_NUMBER, sectionNumber)
            fragment.dummy = sectionNumber
            //fragment.arguments = args
            return fragment
        }
    }


    private fun setAdapter(list: MutableList<JobData>, filter: String) {


        if (filter.equals("")) {

            if (sv_job.visibility != View.VISIBLE && tv_error_internet.visibility != View.VISIBLE) {

                if (list.isEmpty()) {
                    tv_error_result.visibility = View.VISIBLE
                } else {
                    tv_error_result.visibility = View.GONE
                }
            }

            var myAdapter = RecommendationAdapter(list, context!!,
                { data, pos ->
                    val intent = Intent(context, DetailJobActivity::class.java)
                    intent.putExtra("id", data.id)
                    startActivity(intent)

                }, { data, pos ->
                    var job = data
                    if (job.isFav!!) {
                        job.isFav = false
                    } else {
                        job.isFav = true
                    }

                    JobHelper().save(job)
                    setAdapter(list, filter)
                })

            myAdapter.notifyDataSetChanged()
            rv_recomendation.adapter = myAdapter
            val gridLayoutManager = object : GridLayoutManager(context, 2) {}
            rv_recomendation.layoutManager = gridLayoutManager
            rv_recomendation.setHasFixedSize(true)
            rv_recomendation.isFocusable = false

        } else {
            var filteredList: MutableList<JobData> = mutableListOf()
            for (i in list.indices) {
                if (list[i].companyName!!.contains(filter, true)) {
                    filteredList.add(list[i])
                }
            }

            if (filteredList.isEmpty()) {
                tv_error_result.visibility = View.VISIBLE
            } else {
                tv_error_result.visibility = View.GONE
            }

            var myAdapter = RecommendationAdapter(filteredList, context!!,
                { data, pos ->
                    val intent = Intent(context, DetailJobActivity::class.java)
                    intent.putExtra("id", data.id)
                    startActivity(intent)

                }, { data, pos ->
                    var job = data
                    if (job.isFav!!) {
                        job.isFav = false
                    } else {
                        job.isFav = true
                    }

                    JobHelper().save(job)
                    setAdapter(filteredList, filter)
                })

            myAdapter.notifyDataSetChanged()
            rv_recomendation.adapter = myAdapter
            val gridLayoutManager = object : GridLayoutManager(context, 2) {}
            rv_recomendation.layoutManager = gridLayoutManager
            rv_recomendation.setHasFixedSize(true)
            rv_recomendation.isFocusable = false

        }


    }

    internal inner class StringDateComparator : Comparator<JobData> {


        var dateFormat = SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss+SSS")
        override fun compare(lhs: JobData, rhs: JobData): Int {
            return dateFormat.parse(lhs.createdAt).compareTo(dateFormat.parse(rhs.createdAt))
        }
    }

    fun onResult(response: JobResponse) {

        tv_error_internet.visibility = View.GONE
        Helpers.stopShimmerView(sv_job, rv_recomendation)
        list.clear()
        list.addAll(presenter.getAllJob())

        list.sortedWith(StringDateComparator())
        if (list.isNotEmpty()) {
            for (i in response.jobData.indices) {
                // var list = presenter.getAllJob()
                var isNew = true
                if (list.isNotEmpty()) {
                    for (j in list.indices) {
                        if (response.jobData[i].id == (list[j].id)) {
                            var jobData = JobData()
                            jobData = response.jobData[i]
                            jobData.different = list[j].different
                            jobData.isFav = list[j].isFav
                            JobHelper().save(jobData)
                        }
                    }

                }
            }
            setAdapter(list, dummy)
        } else {
            for (i in response.jobData.indices) {
                var job = response.jobData[i]
                job.different =
                        Helpers.differentDay(job.createdAt!!, Helpers.getDateNowByPatterns("yyyy-mm-dd'T'hh:mm:ss+SSS"))
                JobHelper().save(job)
            }
            //JobHelper().saveAndClear(response.jobData)
            var list: MutableList<JobData> = mutableListOf()
            list.addAll(presenter.getAllJob())
            setAdapter(list, dummy)
        }


        //Helpers.showToast(this.requireContext(), response.jobData!![0]!!.jobTitle!!,true)


    }

    fun onError(message: String) {

        Helpers.stopShimmerView(sv_job, rv_recomendation)
        //Helpers.showToast(this.requireContext(),message,true)
        tv_error_internet.visibility = View.VISIBLE
        if(tv_error_result.visibility == View.VISIBLE){
            tv_error_result.visibility = View.GONE
        }
        list.clear()
        setAdapter(list,"")

    }
}
