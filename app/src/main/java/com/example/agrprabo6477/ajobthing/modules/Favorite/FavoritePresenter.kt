package com.example.agrprabo6477.ajobthing.modules.Favorite

import com.example.agrprabo6477.ajobthing.models.response.JobData
import com.example.agrprabo6477.ajobthing.modules.Recommendation.RecomendationFragment
import com.example.agrprabo6477.ajobthing.utils.JobHelper
import com.example.agrprabo6477.ajobthing.utils.PreferenceManager
import id.astra.daihatsu.mobileservice.networking.NetworkModule
import io.reactivex.disposables.CompositeDisposable
import io.realm.Realm

class FavoritePresenter (val v: FavoriteFragment, mRealm: Realm)  {

    private var service: NetworkModule? = null
    private val mCompositeDisposable: CompositeDisposable?
    private val realm = mRealm

    private val preferenceManager = PreferenceManager.instance

    init {
        this.mCompositeDisposable = CompositeDisposable()
    }

    fun getAllJob() : List<JobData> {
        var list : List<JobData> = arrayListOf()
        if(JobHelper().getAll(realm).isNotEmpty()){
            list = JobHelper().getAll(realm)
        }
        return list
    }
}