package com.example.agrprabo6477.ajobthing.models.response

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.RealmClass


@RealmClass
open class Data: RealmObject() {
    @SerializedName("city")
    var city: String? = ""
    @SerializedName("company_name")
    var companyName: String? = ""
    @SerializedName("country")
    var country: String? = ""
    @SerializedName("created_at")
    var createdAt: String? = ""
    @SerializedName("description")
    var description: String? =""
    @SerializedName("id")
    var id: Int? = 0
    @SerializedName("job_title")
    var jobTitle: String? = ""
    @SerializedName("job_type")
    var jobType: String? = ""
    @SerializedName("logo")
    var logo: String? = ""
    @SerializedName("requirement")
    var requirement: String? = ""
    @SerializedName("responsibility")
    var responsibility: String? = ""
    @SerializedName("salary")
    var salary: Salary? = Salary()
    @SerializedName("share_url")
    var shareUrl: String? = ""
}