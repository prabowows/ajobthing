package com.example.agrprabo6477.ajobthing.models

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class SearchViewModel: ViewModel() {
    val currentFilterText: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }
}