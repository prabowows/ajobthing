package com.example.agrprabo6477.ajobthing.models.response

import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.RealmClass


open class AvailableJobResponse  {
    @SerializedName("data")
    var data: RealmList<JobData>? = RealmList()
    @SerializedName("header")
    var header: String? = ""
    @SerializedName("status")
    var status: String? = ""
}