package com.example.agrprabo6477.ajobthing.models.response


import com.google.gson.annotations.SerializedName

data class Meta(
    @SerializedName("hits")
    var hits: Int?,
    @SerializedName("offset")
    var offset: Int?,
    @SerializedName("time")
    var time: Int?
)