package com.example.agrprabo6477.ajobthing.modules


import android.arch.lifecycle.ViewModelProviders
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.example.agrprabo6477.ajobthing.R
import com.example.agrprabo6477.ajobthing.models.SearchViewModel
import com.example.agrprabo6477.ajobthing.modules.Favorite.FavoriteFragment
import com.example.agrprabo6477.ajobthing.modules.Recommendation.RecomendationFragment
import com.jakewharton.rxbinding.widget.RxTextView

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_main.view.*
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v4.app.FragmentStatePagerAdapter].
     */
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private lateinit var filterViewModel: SearchViewModel
    var CURRENT_ACTIVE_FRAGMENT: String = ""
    var EVENT_SEARCH_KEY: String = ""
    var NEWS_SEARCH_KEY: String = ""
    var FORUM_SEARCH_KEY: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        filterViewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        filterViewModel.currentFilterText.value = ""

        //setSupportActionBar(toolbar)
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        container.adapter = mSectionsPagerAdapter

        //et_search.addTextChangedListener(this)


        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))
        CURRENT_ACTIVE_FRAGMENT = RecomendationFragment::class.java.simpleName

        RxTextView.textChangeEvents(et_search)
            .debounce(
                500,
                TimeUnit.MILLISECONDS
            ) // Better store the value in a constant like Constant.DEBOUNCE_SEARCH_REQUEST_TIMEOUT
//            .map { it.text().toString() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (et_search.isFocusable) {
                    filterViewModel.currentFilterText.value = it.text().toString()
                    when (CURRENT_ACTIVE_FRAGMENT) {
                        RecomendationFragment::class.java.simpleName -> {
                            EVENT_SEARCH_KEY = it.text().toString()
                        }
                        FavoriteFragment::class.java.simpleName -> {
                            NEWS_SEARCH_KEY = it.text().toString()
                        }

                    }
                }
            }

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }


    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? = when (position) {
            0 -> {
                CURRENT_ACTIVE_FRAGMENT = RecomendationFragment::class.java.simpleName
                et_search.setText(EVENT_SEARCH_KEY)
                RecomendationFragment.newInstance(
                    ""
                )
            }
            1
            -> {
                CURRENT_ACTIVE_FRAGMENT = FavoriteFragment::class.java.simpleName
                et_search.setText(NEWS_SEARCH_KEY)
                FavoriteFragment.newInstance(
                    position + 10
                )
            }
            //2 -> FragmentThree.newInstance()
            else -> null
        }

        override fun getCount(): Int {

            return 2
        }
    }


}
