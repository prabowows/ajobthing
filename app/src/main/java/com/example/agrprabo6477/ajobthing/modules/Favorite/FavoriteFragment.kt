package com.example.agrprabo6477.ajobthing.modules.Favorite

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.agrprabo6477.ajobthing.R
import com.example.agrprabo6477.ajobthing.models.SearchViewModel
import com.example.agrprabo6477.ajobthing.models.response.JobData
import com.example.agrprabo6477.ajobthing.modules.DetailJob.DetailJobActivity
import com.example.agrprabo6477.ajobthing.modules.MainActivity
import com.example.agrprabo6477.ajobthing.modules.Recommendation.RecomendationFragment
import com.example.agrprabo6477.ajobthing.modules.Recommendation.RecommendationAdapter
import com.example.agrprabo6477.ajobthing.utils.JobHelper
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.fragment_second.*
import id.ai.mobilerecruitment.utils.Helpers
class FavoriteFragment : Fragment() {

    private lateinit var presenter: FavoritePresenter
    private lateinit var realm : Realm
    private lateinit var filterViewModel: SearchViewModel
    private var list : List<JobData> = arrayListOf()
    private var filteredList : MutableList<JobData> = arrayListOf()
    private var search = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_second, container, false)
        //rootView.section_label.text = getString(R.string.section_format, arguments?.getInt(ARG_SECTION_NUMBER))
        return rootView
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(sectionNumber: Int): FavoriteFragment {
            val fragment = FavoriteFragment()
            val args = Bundle()
            args.putInt(ARG_SECTION_NUMBER, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        realm = Realm.getDefaultInstance()
        presenter = FavoritePresenter(this,realm)

        var list = presenter.getAllJob()
        var filteredList : MutableList<JobData> = mutableListOf()
        for (i in list.indices){
            if(list[i].isFav!!){
                filteredList.add(list[i])
            }
        }
        setAdapter(filteredList,"")
    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)

        if(menuVisible){
            filteredList.clear()
            list = presenter.getAllJob()
            for (i in list.indices){
                if(list[i].isFav!!){
                    filteredList.add(list[i])
                }
            }
            setAdapter(filteredList,search)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as MainActivity).let {
            filterViewModel = ViewModelProviders.of(it).get(SearchViewModel::class.java)
            filterViewModel.currentFilterText.observe(this, Observer {
                it?.let {
                    //                    adapter.filter.filter(it)
                    if ((activity as MainActivity).CURRENT_ACTIVE_FRAGMENT.equals(FavoriteFragment::class.java.simpleName, true)) {

                        setAdapter(filteredList,it)
                        search = it
                        //Helpers.showToast(this.requireContext(),it,true)
//                            sv_forum.visibility = View.VISIBLE
//                            list.clear()
//                            page = 1
//                            presenter.hitDataForum(page, keySearch = it)
                    }
                }

            })
        }

    }


    private fun setAdapter(list: MutableList<JobData>, filter : String) {

        if(list.isEmpty()){
            tv_error_saved.visibility = View.VISIBLE
        }
        else {
            tv_error_saved.visibility = View.GONE
        }

        if(filter.equals("")){
            var myAdapter = FavoriteAdapter(list, context!!,
                { data, pos ->
                    val intent = Intent(context, DetailJobActivity::class.java)
                    intent.putExtra("id", data.id)
                    startActivity(intent)

                }, { data, pos ->
                    var job = data
                    if(job.isFav!!){
                        job.isFav = false
                    }else{
                        job.isFav = true
                    }

                    JobHelper().save(job)
                    setAdapter(list,filter)
                })

            myAdapter.notifyDataSetChanged()
            rv_favorite.adapter = myAdapter
            val gridLayoutManager = object : GridLayoutManager(context, 2) {}
            rv_favorite.layoutManager = gridLayoutManager
            rv_favorite.setHasFixedSize(true)
            rv_favorite.isFocusable = false

        }else {
            var filteredList : MutableList<JobData> = mutableListOf()
            for (i in list.indices){
                if(list[i].companyName!!.contains(filter,true)){
                    filteredList.add(list[i])
                }
            }

            var myAdapter = FavoriteAdapter(filteredList, context!!,
                { data, pos ->
                    val intent = Intent(context, DetailJobActivity::class.java)
                    intent.putExtra("id", data.id)
                    startActivity(intent)

                }, { data, pos ->
                    var job = data
                    if(job.isFav!!){
                        job.isFav = false
                    }else{
                        job.isFav = true
                    }

                    JobHelper().save(job)
                    setAdapter(filteredList,filter)
                })

            myAdapter.notifyDataSetChanged()
            rv_favorite.adapter = myAdapter
            val gridLayoutManager = object : GridLayoutManager(context, 2) {}
            rv_favorite.layoutManager = gridLayoutManager
            rv_favorite.setHasFixedSize(true)
            rv_favorite.isFocusable = false

        }



    }
}
