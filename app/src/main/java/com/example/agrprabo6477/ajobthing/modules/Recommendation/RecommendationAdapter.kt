package com.example.agrprabo6477.ajobthing.modules.Recommendation

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.agrprabo6477.ajobthing.R
import com.example.agrprabo6477.ajobthing.models.response.JobData
import kotlinx.android.synthetic.main.item_job.view.*
import id.ai.mobilerecruitment.utils.Helpers

class RecommendationAdapter(
    private val listMenu: MutableList<JobData>,
    private val context: Context,
    private val itemClick: (data: JobData, pos: Int) -> Unit,
    private val loveClick: (data: JobData, pos: Int) -> Unit
) :
    RecyclerView.Adapter<RecommendationAdapter.Holder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_job, parent, false)

        return Holder(view!!, context, itemClick,loveClick)
    }

    override fun getItemCount(): Int {
        return listMenu.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.Bind(listMenu[position], position)
    }

    class Holder(
        view: View,
        val context: Context,
        val itemClick: (data: JobData, pos: Int) -> Unit,
        val loveClick: (data: JobData, pos: Int) -> Unit
    ) :
        RecyclerView.ViewHolder(view) {


        fun Bind(data: JobData, pos: Int) {
            with(data) {
                Helpers.showClearImageFromURL(context,data.logo!!,itemView.circleImageView)
                itemView.tv_job_title.text = data.companyName
                itemView.tv_position.text = data.jobTitle
                itemView.tv_location.text = "\u2022 ${data.city}"
                itemView.tv_status.text = "\u2022 ${data.jobType}"

                if (data.isFav!!) {
                    itemView.ib_love.setImageDrawable(context.getDrawable(R.drawable.ic_like_filled))
                } else {
                    itemView.ib_love.setImageDrawable(context.getDrawable(R.drawable.ic_like))
                }
                itemView.ib_love.setOnClickListener {
                    loveClick(this, pos)
                }

                itemView.setOnClickListener {
                    itemClick(this,pos)
                }

                var different = Helpers.differentDay(data.createdAt!!,Helpers.getDateNowByPatterns("yyyy-mm-dd'T'hh:mm:ss+SSS"))
                if(different> 365){
                    var result : Int = different/365
                    itemView.tv_posted_date.text = "Posted ${result} years ago"
                }
                else if (different > 30){
                    var result : Int = different/30
                    itemView.tv_posted_date.text = "Posted ${result} months ago"
                }
                else if (different  > 6){
                    var result : Int = different/6
                    itemView.tv_posted_date.text = "Posted ${result} weeks ago"
                }
                else {
                    itemView.tv_posted_date.text = "Posted ${different} days ago"
                }



//                itemView.item_iv_icon_menu.setImageDrawable(android.support.v4.content.ContextCompat.getDrawable(context, data.iconRes))
//                itemView.item_tv_title_menu.text = data.name
//                itemView.item_container_menu.setOnClickListener {
//                    itemClick(this, pos)
//                }
            }
        }
    }


}