package com.example.agrprabo6477.ajobthing.modules.DetailJob

import com.example.agrprabo6477.ajobthing.models.response.JobData
import com.example.agrprabo6477.ajobthing.modules.Favorite.FavoriteFragment
import com.example.agrprabo6477.ajobthing.utils.JobHelper
import com.example.agrprabo6477.ajobthing.utils.PreferenceManager
import id.astra.daihatsu.mobileservice.networking.NetworkModule
import io.reactivex.disposables.CompositeDisposable
import io.realm.Realm

class DetailJobPresenter (val v: DetailJobActivity, mRealm: Realm)  {

    private var service: NetworkModule? = null
    private val mCompositeDisposable: CompositeDisposable?
    private val realm = mRealm

    private val preferenceManager = PreferenceManager.instance

    init {
        this.mCompositeDisposable = CompositeDisposable()
    }

    fun getAllJob() : List<JobData> {
        var list = JobHelper().getAll(realm)
        return list
    }

    fun getJob(id : Int) : JobData {
        var job = JobData()
        if(JobHelper().getJobById(realm,id) != null){
            job = JobHelper().getJobById(realm,id)!!
        }

        return job
    }
}