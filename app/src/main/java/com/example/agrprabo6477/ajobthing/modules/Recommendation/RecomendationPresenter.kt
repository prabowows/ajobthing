package com.example.agrprabo6477.ajobthing.modules.Recommendation

import com.example.agrprabo6477.ajobthing.models.response.AvailableJobResponse
import com.example.agrprabo6477.ajobthing.models.response.JobData
import com.example.agrprabo6477.ajobthing.models.response.JobResponse
import com.example.agrprabo6477.ajobthing.utils.JobHelper
import com.example.agrprabo6477.ajobthing.utils.PreferenceManager
import id.ai.mobilerecruitment.utils.Helpers
import id.astra.ai.utils.Const
import id.astra.daihatsu.mobileservice.networking.NetworkHelper
import id.astra.daihatsu.mobileservice.networking.NetworkModule
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm

class RecomendationPresenter(val v: RecomendationFragment, mRealm: Realm) {

    private var service: NetworkModule? = null
    private val mCompositeDisposable: CompositeDisposable?
    private val realm = mRealm

    private val preferenceManager = PreferenceManager.instance

    init {
        this.mCompositeDisposable = CompositeDisposable()
    }


    fun hitGetData() {

        try {
            //val requestToJson: String = Helpers.getDefaultGson(v.requireContext()).toJson(request)

            this.service = NetworkModule(
                v.requireContext()
            )
            val responseObservable = service!!.getPreparedObservable(
                service!!.service.GetAvailableJob(), JobResponse::class.java, false, false
            )
            mCompositeDisposable!!.add(
                responseObservable
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ this.handleResponse(it) }, { this.handleResponse(it) })
            )
        } catch (e: Exception) {
            e.printStackTrace()
            //Crashlytics.logException(e)
        }
    }


    fun handleResponse(response: Any) {
        try {
            if (response is JobResponse) {
                if (response.status.equals("success",true)){

                    v.onResult(response)

                } else {
                    v.onError(response.status!!)
                }
            } else if (response is Throwable) {
                v.onError(response.message!!)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getAllJob() : List<JobData> {
        var list : List<JobData> = arrayListOf()
        if(JobHelper().getAll(realm).isNotEmpty()){
            list = JobHelper().getAll(realm)
        }
        return list
    }




    fun rxUnSubscribe() {
        mCompositeDisposable?.clear()
    }

}